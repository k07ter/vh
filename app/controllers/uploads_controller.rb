class UploadsController < ApplicationController
  #before_action :authenticate_auth!

  #def av_will_change
  #    @current_drafter
  #end

  def employee
    @employee = Employee.find(params[:id])
    employee = @employee.update(profile: params[:attachments].first)

    if employee.save
      employee_url = employee.av.thumb.url
      current_auth.update(employee_id: employee.id, employee_url: employee_url)
      render json: Oj.dump([employee_url])
    else
      render json: {msg: employee.errors.full_messagges.join(", ")}
    end

  def visitor
    @visitor = Visitor.find(params[:id])
    visitor = @visitor.update(avatar_file_name: params[:attachments].first)

    if visitor.save
      visitor_url = visitor.avatar_file_name.thumb.url
      current_visitor.update(visitor_id: visitor.id, visitor_url: visitor_url)
      render json: Oj.dump([visitor_url])
    else
      render json: {msg: visitor.errors.full_messagges.join(", ")}
    end

end

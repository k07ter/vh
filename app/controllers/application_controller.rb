class ApplicationController < ActionController::Base
  #around_action :srvtm
  #,:with_time_zone, if: 'current_visitor.try(:time_zone)'

  protect_from_forgery with: :null_session  #exception
  
  #protected

  #def with_time_zone(&block)
    #time_zone = current_visitor.time_zone
    #logger.debug "Используется часовой пояс пользователя: #(time_zone)"
    #Time.use_zone(time_zone, &block)
  #end

  def index
    #respond_to do |format|
    #  format.json { render :srvtm => "Текущее время: {Time.current.to_s}" }
    #end
    @calend = l(Time.current, format: '%d %B %Y, %T')
    gon.watch.calend = @calend
    #respond_to do |format|
    #  format.html { redirect_to @calend }
    #end
    #render :index => 'home/index_bdy', :locale => :ru
    #, :calend => @calend
  end

end

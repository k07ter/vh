json.extract! employee, :id, :name, :lastname, :login, :password, :email, :phone, :profile, :created_at, :updated_at
json.url employee_url(employee, format: :json)

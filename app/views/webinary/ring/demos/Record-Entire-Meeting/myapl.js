// 1st step
var NodeJsRecordingHandler = require('./Nodejs-Recording-Handler.js');

io.on('connection', function(socket) {
    // 2nd & last step:
    // call below line for each socket connection
    // it will never affect your other socket.io events or objects
    NodeJsRecordingHandler(socket);

    // your custom socket.io code goes here
});
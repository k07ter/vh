json.extract! visitor, :id, :name, :lastname, :login, :password, :email, :phone, :tip, :education, :occupation, :subsection, :rating, :injob, :outjob, :startmeeting, :endmeeting, :created_at, :updated_at, :avatar_file_name
json.url visitor_url(visitor, format: :json)

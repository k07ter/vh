class Employee < ApplicationRecord
  #establish_connection :adapter => 'postgresql', :database => 'control', :username => 'light', :password => 'p12345678'
  mount_uploader :profile, EmployeeUploader

  #has_attached_file :av
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
end

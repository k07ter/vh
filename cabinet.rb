deploy_to = /root/ruby/cabinet
rails_root = "#{deploy_to}/current"
pid_file = "#{deploy_to}/pids/cabinet.pid"
socket_file = "#{deploy_to}/cabinet.sock"
log_file = "#{rails_root}/logs/cabinet.log"
err_log = "#{rails_root}/logs/cabinet_err.log"
old_pid = pid_file + '.oldbin'

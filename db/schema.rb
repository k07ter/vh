# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170820111145) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "employees", force: :cascade do |t|
    t.string "name"
    t.string "lastname"
    t.string "login"
    t.string "password"
    t.string "email"
    t.string "phone"
    t.string "tip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "profile"
    t.index ["email"], name: "index_employees_on_email", unique: true
    t.index ["reset_password_token"], name: "index_employees_on_reset_password_token", unique: true
  end

  create_table "meetings", force: :cascade do |t|
    t.string "name"
    t.datetime "start_time"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "phpbb_acl_groups", id: false, force: :cascade do |t|
    t.integer "group_id", default: 0, null: false
    t.integer "forum_id", default: 0, null: false
    t.integer "auth_option_id", default: 0, null: false
    t.integer "auth_role_id", default: 0, null: false
    t.integer "auth_setting", limit: 2, null: false
    t.index ["auth_option_id"], name: "phpbb_acl_groups_auth_opt_id"
    t.index ["auth_role_id"], name: "phpbb_acl_groups_auth_role_id"
    t.index ["group_id"], name: "phpbb_acl_groups_group_id"
  end

  create_table "phpbb_acl_options", primary_key: "auth_option_id", id: :integer, default: -> { "nextval('phpbb_acl_options_seq'::regclass)" }, force: :cascade do |t|
    t.string "auth_option", limit: 50, default: "", null: false
    t.integer "is_global", limit: 2, null: false
    t.integer "is_local", limit: 2, null: false
    t.integer "founder_only", limit: 2, null: false
    t.index ["auth_option"], name: "phpbb_acl_options_auth_option", unique: true
  end

  create_table "phpbb_acl_roles", primary_key: "role_id", id: :integer, default: -> { "nextval('phpbb_acl_roles_seq'::regclass)" }, force: :cascade do |t|
    t.string "role_name", limit: 255, default: "", null: false
    t.string "role_description", limit: 4000, default: "", null: false
    t.string "role_type", limit: 10, default: "", null: false
    t.integer "role_order", limit: 2, null: false
    t.index ["role_order"], name: "phpbb_acl_roles_role_order"
    t.index ["role_type"], name: "phpbb_acl_roles_role_type"
  end

  create_table "phpbb_acl_roles_data", primary_key: ["role_id", "auth_option_id"], force: :cascade do |t|
    t.integer "role_id", default: 0, null: false
    t.integer "auth_option_id", default: 0, null: false
    t.integer "auth_setting", limit: 2, null: false
    t.index ["auth_option_id"], name: "phpbb_acl_roles_data_ath_op_id"
  end

  create_table "phpbb_acl_users", id: false, force: :cascade do |t|
    t.integer "user_id", default: 0, null: false
    t.integer "forum_id", default: 0, null: false
    t.integer "auth_option_id", default: 0, null: false
    t.integer "auth_role_id", default: 0, null: false
    t.integer "auth_setting", limit: 2, null: false
    t.index ["auth_option_id"], name: "phpbb_acl_users_auth_option_id"
    t.index ["auth_role_id"], name: "phpbb_acl_users_auth_role_id"
    t.index ["user_id"], name: "phpbb_acl_users_user_id"
  end

  create_table "phpbb_attachments", primary_key: "attach_id", id: :integer, default: -> { "nextval('phpbb_attachments_seq'::regclass)" }, force: :cascade do |t|
    t.integer "post_msg_id", default: 0, null: false
    t.integer "topic_id", default: 0, null: false
    t.integer "in_message", limit: 2, null: false
    t.integer "poster_id", default: 0, null: false
    t.integer "is_orphan", limit: 2, null: false
    t.string "physical_filename", limit: 255, default: "", null: false
    t.string "real_filename", limit: 255, default: "", null: false
    t.integer "download_count", default: 0, null: false
    t.string "attach_comment", limit: 4000, default: "", null: false
    t.string "extension", limit: 100, default: "", null: false
    t.string "mimetype", limit: 100, default: "", null: false
    t.integer "filesize", default: 0, null: false
    t.integer "filetime", default: 0, null: false
    t.integer "thumbnail", limit: 2, null: false
    t.index ["filetime"], name: "phpbb_attachments_filetime"
    t.index ["is_orphan"], name: "phpbb_attachments_is_orphan"
    t.index ["post_msg_id"], name: "phpbb_attachments_post_msg_id"
    t.index ["poster_id"], name: "phpbb_attachments_poster_id"
    t.index ["topic_id"], name: "phpbb_attachments_topic_id"
  end

  create_table "phpbb_banlist", primary_key: "ban_id", id: :integer, default: -> { "nextval('phpbb_banlist_seq'::regclass)" }, force: :cascade do |t|
    t.integer "ban_userid", default: 0, null: false
    t.string "ban_ip", limit: 40, default: "", null: false
    t.string "ban_email", limit: 100, default: "", null: false
    t.integer "ban_start", default: 0, null: false
    t.integer "ban_end", default: 0, null: false
    t.integer "ban_exclude", limit: 2, null: false
    t.string "ban_reason", limit: 255, default: "", null: false
    t.string "ban_give_reason", limit: 255, default: "", null: false
    t.index ["ban_email", "ban_exclude"], name: "phpbb_banlist_ban_email"
    t.index ["ban_end"], name: "phpbb_banlist_ban_end"
    t.index ["ban_ip", "ban_exclude"], name: "phpbb_banlist_ban_ip"
    t.index ["ban_userid", "ban_exclude"], name: "phpbb_banlist_ban_user"
  end

  create_table "phpbb_bbcodes", primary_key: "bbcode_id", id: :integer, limit: 2, default: nil, force: :cascade do |t|
    t.string "bbcode_tag", limit: 16, default: "", null: false
    t.string "bbcode_helpline", limit: 255, default: "", null: false
    t.integer "display_on_posting", limit: 2, null: false
    t.string "bbcode_match", limit: 4000, default: "", null: false
    t.text "bbcode_tpl", default: "", null: false
    t.text "first_pass_match", default: "", null: false
    t.text "first_pass_replace", default: "", null: false
    t.text "second_pass_match", default: "", null: false
    t.text "second_pass_replace", default: "", null: false
    t.index ["display_on_posting"], name: "phpbb_bbcodes_display_on_post"
  end

  create_table "phpbb_bookmarks", primary_key: ["topic_id", "user_id"], force: :cascade do |t|
    t.integer "topic_id", default: 0, null: false
    t.integer "user_id", default: 0, null: false
  end

  create_table "phpbb_bots", primary_key: "bot_id", id: :integer, default: -> { "nextval('phpbb_bots_seq'::regclass)" }, force: :cascade do |t|
    t.integer "bot_active", limit: 2, null: false
    t.string "bot_name", limit: 255, default: "", null: false
    t.integer "user_id", default: 0, null: false
    t.string "bot_agent", limit: 255, default: "", null: false
    t.string "bot_ip", limit: 255, default: "", null: false
    t.index ["bot_active"], name: "phpbb_bots_bot_active"
  end

  create_table "phpbb_config", primary_key: "config_name", id: :string, limit: 255, default: "", force: :cascade do |t|
    t.string "config_value", limit: 255, default: "", null: false
    t.integer "is_dynamic", limit: 2, null: false
    t.index ["is_dynamic"], name: "phpbb_config_is_dynamic"
  end

  create_table "phpbb_config_text", primary_key: "config_name", id: :string, limit: 255, default: "", force: :cascade do |t|
    t.text "config_value", default: "", null: false
  end

  create_table "phpbb_confirm", primary_key: ["session_id", "confirm_id"], force: :cascade do |t|
    t.string "confirm_id", limit: 32, default: "", null: false
    t.string "session_id", limit: 32, default: "", null: false
    t.integer "confirm_type", limit: 2, null: false
    t.string "code", limit: 8, default: "", null: false
    t.integer "seed", default: 0, null: false
    t.integer "attempts", default: 0, null: false
    t.index ["confirm_type"], name: "phpbb_confirm_confirm_type"
  end

  create_table "phpbb_disallow", primary_key: "disallow_id", id: :integer, default: -> { "nextval('phpbb_disallow_seq'::regclass)" }, force: :cascade do |t|
    t.string "disallow_username", limit: 255, default: "", null: false
  end

  create_table "phpbb_drafts", primary_key: "draft_id", id: :integer, default: -> { "nextval('phpbb_drafts_seq'::regclass)" }, force: :cascade do |t|
    t.integer "user_id", default: 0, null: false
    t.integer "topic_id", default: 0, null: false
    t.integer "forum_id", default: 0, null: false
    t.integer "save_time", default: 0, null: false
    t.string "draft_subject", limit: 255, default: "", null: false
    t.text "draft_message", default: "", null: false
    t.index ["save_time"], name: "phpbb_drafts_save_time"
  end

  create_table "phpbb_ext", id: false, force: :cascade do |t|
    t.string "ext_name", limit: 255, default: "", null: false
    t.integer "ext_active", limit: 2, null: false
    t.string "ext_state", limit: 8000, default: "", null: false
    t.index ["ext_name"], name: "phpbb_ext_ext_name", unique: true
  end

  create_table "phpbb_extension_groups", primary_key: "group_id", id: :integer, default: -> { "nextval('phpbb_extension_groups_seq'::regclass)" }, force: :cascade do |t|
    t.string "group_name", limit: 255, default: "", null: false
    t.integer "cat_id", limit: 2, null: false
    t.integer "allow_group", limit: 2, null: false
    t.integer "download_mode", limit: 2, null: false
    t.string "upload_icon", limit: 255, default: "", null: false
    t.integer "max_filesize", default: 0, null: false
    t.string "allowed_forums", limit: 8000, default: "", null: false
    t.integer "allow_in_pm", limit: 2, null: false
  end

  create_table "phpbb_extensions", primary_key: "extension_id", id: :integer, default: -> { "nextval('phpbb_extensions_seq'::regclass)" }, force: :cascade do |t|
    t.integer "group_id", default: 0, null: false
    t.string "extension", limit: 100, default: "", null: false
  end

  create_table "phpbb_forums", primary_key: "forum_id", id: :integer, default: -> { "nextval('phpbb_forums_seq'::regclass)" }, force: :cascade do |t|
    t.integer "parent_id", default: 0, null: false
    t.integer "left_id", default: 0, null: false
    t.integer "right_id", default: 0, null: false
    t.text "forum_parents", default: "", null: false
    t.string "forum_name", limit: 255, default: "", null: false
    t.string "forum_desc", limit: 4000, default: "", null: false
    t.string "forum_desc_bitfield", limit: 255, default: "", null: false
    t.integer "forum_desc_options", default: 7, null: false
    t.string "forum_desc_uid", limit: 8, default: "", null: false
    t.string "forum_link", limit: 255, default: "", null: false
    t.string "forum_password", limit: 255, default: "", null: false
    t.integer "forum_style", default: 0, null: false
    t.string "forum_image", limit: 255, default: "", null: false
    t.string "forum_rules", limit: 4000, default: "", null: false
    t.string "forum_rules_link", limit: 255, default: "", null: false
    t.string "forum_rules_bitfield", limit: 255, default: "", null: false
    t.integer "forum_rules_options", default: 7, null: false
    t.string "forum_rules_uid", limit: 8, default: "", null: false
    t.integer "forum_topics_per_page", limit: 2, null: false
    t.integer "forum_type", limit: 2, null: false
    t.integer "forum_status", limit: 2, null: false
    t.integer "forum_last_post_id", default: 0, null: false
    t.integer "forum_last_poster_id", default: 0, null: false
    t.string "forum_last_post_subject", limit: 255, default: "", null: false
    t.integer "forum_last_post_time", default: 0, null: false
    t.string "forum_last_poster_name", limit: 255, default: "", null: false
    t.string "forum_last_poster_colour", limit: 6, default: "", null: false
    t.integer "forum_flags", limit: 2, null: false
    t.integer "display_on_index", limit: 2, null: false
    t.integer "enable_indexing", limit: 2, null: false
    t.integer "enable_icons", limit: 2, null: false
    t.integer "enable_prune", limit: 2, null: false
    t.integer "prune_next", default: 0, null: false
    t.integer "prune_days", default: 0, null: false
    t.integer "prune_viewed", default: 0, null: false
    t.integer "prune_freq", default: 0, null: false
    t.integer "display_subforum_list", limit: 2, null: false
    t.integer "forum_options", default: 0, null: false
    t.integer "forum_posts_approved", default: 0, null: false
    t.integer "forum_posts_unapproved", default: 0, null: false
    t.integer "forum_posts_softdeleted", default: 0, null: false
    t.integer "forum_topics_approved", default: 0, null: false
    t.integer "forum_topics_unapproved", default: 0, null: false
    t.integer "forum_topics_softdeleted", default: 0, null: false
    t.integer "enable_shadow_prune", limit: 2, null: false
    t.integer "prune_shadow_days", default: 7, null: false
    t.integer "prune_shadow_freq", default: 1, null: false
    t.integer "prune_shadow_next", default: 0, null: false
    t.index ["forum_last_post_id"], name: "phpbb_forums_forum_lastpost_id"
    t.index ["left_id", "right_id"], name: "phpbb_forums_left_right_id"
  end

  create_table "phpbb_forums_access", primary_key: ["forum_id", "user_id", "session_id"], force: :cascade do |t|
    t.integer "forum_id", default: 0, null: false
    t.integer "user_id", default: 0, null: false
    t.string "session_id", limit: 32, default: "", null: false
  end

  create_table "phpbb_forums_track", primary_key: ["user_id", "forum_id"], force: :cascade do |t|
    t.integer "user_id", default: 0, null: false
    t.integer "forum_id", default: 0, null: false
    t.integer "mark_time", default: 0, null: false
  end

  create_table "phpbb_forums_watch", id: false, force: :cascade do |t|
    t.integer "forum_id", default: 0, null: false
    t.integer "user_id", default: 0, null: false
    t.integer "notify_status", limit: 2, null: false
    t.index ["forum_id"], name: "phpbb_forums_watch_forum_id"
    t.index ["notify_status"], name: "phpbb_forums_watch_notify_stat"
    t.index ["user_id"], name: "phpbb_forums_watch_user_id"
  end

  create_table "phpbb_groups", primary_key: "group_id", id: :integer, default: -> { "nextval('phpbb_groups_seq'::regclass)" }, force: :cascade do |t|
    t.integer "group_type", limit: 2, null: false
    t.integer "group_founder_manage", limit: 2, null: false
    t.integer "group_skip_auth", limit: 2, null: false
    t.string "group_name", default: "", null: false
    t.string "group_desc", limit: 4000, default: "", null: false
    t.string "group_desc_bitfield", limit: 255, default: "", null: false
    t.integer "group_desc_options", default: 7, null: false
    t.string "group_desc_uid", limit: 8, default: "", null: false
    t.integer "group_display", limit: 2, null: false
    t.string "group_avatar", limit: 255, default: "", null: false
    t.string "group_avatar_type", limit: 255, default: "", null: false
    t.integer "group_avatar_width", limit: 2, null: false
    t.integer "group_avatar_height", limit: 2, null: false
    t.integer "group_rank", default: 0, null: false
    t.string "group_colour", limit: 6, default: "", null: false
    t.integer "group_sig_chars", default: 0, null: false
    t.integer "group_receive_pm", limit: 2, null: false
    t.integer "group_message_limit", default: 0, null: false
    t.integer "group_legend", default: 0, null: false
    t.integer "group_max_recipients", default: 0, null: false
    t.index ["group_legend", "group_name"], name: "phpbb_groups_group_legend_name"
  end

  create_table "phpbb_icons", primary_key: "icons_id", id: :integer, default: -> { "nextval('phpbb_icons_seq'::regclass)" }, force: :cascade do |t|
    t.string "icons_url", limit: 255, default: "", null: false
    t.integer "icons_width", limit: 2, null: false
    t.integer "icons_height", limit: 2, null: false
    t.integer "icons_order", default: 0, null: false
    t.integer "display_on_posting", limit: 2, null: false
    t.index ["display_on_posting"], name: "phpbb_icons_display_on_posting"
  end

  create_table "phpbb_lang", primary_key: "lang_id", id: :integer, limit: 2, default: -> { "nextval('phpbb_lang_seq'::regclass)" }, force: :cascade do |t|
    t.string "lang_iso", limit: 30, default: "", null: false
    t.string "lang_dir", limit: 30, default: "", null: false
    t.string "lang_english_name", limit: 100, default: "", null: false
    t.string "lang_local_name", limit: 255, default: "", null: false
    t.string "lang_author", limit: 255, default: "", null: false
    t.index ["lang_iso"], name: "phpbb_lang_lang_iso"
  end

  create_table "phpbb_log", primary_key: "log_id", id: :integer, default: -> { "nextval('phpbb_log_seq'::regclass)" }, force: :cascade do |t|
    t.integer "log_type", limit: 2, null: false
    t.integer "user_id", default: 0, null: false
    t.integer "forum_id", default: 0, null: false
    t.integer "topic_id", default: 0, null: false
    t.integer "reportee_id", default: 0, null: false
    t.string "log_ip", limit: 40, default: "", null: false
    t.integer "log_time", default: 0, null: false
    t.string "log_operation", limit: 4000, default: "", null: false
    t.text "log_data", default: "", null: false
    t.index ["forum_id"], name: "phpbb_log_forum_id"
    t.index ["log_type"], name: "phpbb_log_log_type"
    t.index ["reportee_id"], name: "phpbb_log_reportee_id"
    t.index ["topic_id"], name: "phpbb_log_topic_id"
    t.index ["user_id"], name: "phpbb_log_user_id"
  end

  create_table "phpbb_login_attempts", id: false, force: :cascade do |t|
    t.string "attempt_ip", limit: 40, default: "", null: false
    t.string "attempt_browser", limit: 150, default: "", null: false
    t.string "attempt_forwarded_for", limit: 255, default: "", null: false
    t.integer "attempt_time", default: 0, null: false
    t.integer "user_id", default: 0, null: false
    t.string "username", limit: 255, default: "0", null: false
    t.string "username_clean", default: "0", null: false
    t.index ["attempt_forwarded_for", "attempt_time"], name: "phpbb_login_attempts_att_for"
    t.index ["attempt_ip", "attempt_time"], name: "phpbb_login_attempts_att_ip"
    t.index ["attempt_time"], name: "phpbb_login_attempts_att_time"
    t.index ["user_id"], name: "phpbb_login_attempts_user_id"
  end

  create_table "phpbb_migrations", primary_key: "migration_name", id: :string, limit: 255, default: "", force: :cascade do |t|
    t.string "migration_depends_on", limit: 8000, default: "", null: false
    t.integer "migration_schema_done", limit: 2, null: false
    t.integer "migration_data_done", limit: 2, null: false
    t.string "migration_data_state", limit: 8000, default: "", null: false
    t.integer "migration_start_time", default: 0, null: false
    t.integer "migration_end_time", default: 0, null: false
  end

  create_table "phpbb_moderator_cache", id: false, force: :cascade do |t|
    t.integer "forum_id", default: 0, null: false
    t.integer "user_id", default: 0, null: false
    t.string "username", limit: 255, default: "", null: false
    t.integer "group_id", default: 0, null: false
    t.string "group_name", limit: 255, default: "", null: false
    t.integer "display_on_index", limit: 2, null: false
    t.index ["display_on_index"], name: "phpbb_moderator_cache_disp_idx"
    t.index ["forum_id"], name: "phpbb_moderator_cache_forum_id"
  end

  create_table "phpbb_modules", primary_key: "module_id", id: :integer, default: -> { "nextval('phpbb_modules_seq'::regclass)" }, force: :cascade do |t|
    t.integer "module_enabled", limit: 2, null: false
    t.integer "module_display", limit: 2, null: false
    t.string "module_basename", limit: 255, default: "", null: false
    t.string "module_class", limit: 10, default: "", null: false
    t.integer "parent_id", default: 0, null: false
    t.integer "left_id", default: 0, null: false
    t.integer "right_id", default: 0, null: false
    t.string "module_langname", limit: 255, default: "", null: false
    t.string "module_mode", limit: 255, default: "", null: false
    t.string "module_auth", limit: 255, default: "", null: false
    t.index ["left_id", "right_id"], name: "phpbb_modules_left_right_id"
    t.index ["module_class", "left_id"], name: "phpbb_modules_class_left_id"
    t.index ["module_enabled"], name: "phpbb_modules_module_enabled"
  end

  create_table "phpbb_notification_types", primary_key: "notification_type_id", id: :integer, limit: 2, default: -> { "nextval('phpbb_notification_types_seq'::regclass)" }, force: :cascade do |t|
    t.string "notification_type_name", limit: 255, default: "", null: false
    t.integer "notification_type_enabled", limit: 2, null: false
    t.index ["notification_type_name"], name: "phpbb_notification_types_type", unique: true
  end

  create_table "phpbb_notifications", primary_key: "notification_id", id: :integer, default: -> { "nextval('phpbb_notifications_seq'::regclass)" }, force: :cascade do |t|
    t.integer "notification_type_id", limit: 2, null: false
    t.integer "item_id", default: 0, null: false
    t.integer "item_parent_id", default: 0, null: false
    t.integer "user_id", default: 0, null: false
    t.integer "notification_read", limit: 2, null: false
    t.integer "notification_time", default: 1, null: false
    t.string "notification_data", limit: 4000, default: "", null: false
    t.index ["notification_type_id", "item_id"], name: "phpbb_notifications_item_ident"
    t.index ["user_id", "notification_read"], name: "phpbb_notifications_user"
  end

  create_table "phpbb_oauth_accounts", primary_key: ["user_id", "provider"], force: :cascade do |t|
    t.integer "user_id", default: 0, null: false
    t.string "provider", limit: 255, default: "", null: false
    t.string "oauth_provider_id", limit: 4000, default: "", null: false
  end

  create_table "phpbb_oauth_tokens", id: false, force: :cascade do |t|
    t.integer "user_id", default: 0, null: false
    t.string "session_id", limit: 32, default: "", null: false
    t.string "provider", limit: 255, default: "", null: false
    t.text "oauth_token", default: "", null: false
    t.index ["provider"], name: "phpbb_oauth_tokens_provider"
    t.index ["user_id"], name: "phpbb_oauth_tokens_user_id"
  end

  create_table "phpbb_poll_options", id: false, force: :cascade do |t|
    t.integer "poll_option_id", limit: 2, null: false
    t.integer "topic_id", default: 0, null: false
    t.string "poll_option_text", limit: 4000, default: "", null: false
    t.integer "poll_option_total", default: 0, null: false
    t.index ["poll_option_id"], name: "phpbb_poll_options_poll_opt_id"
    t.index ["topic_id"], name: "phpbb_poll_options_topic_id"
  end

  create_table "phpbb_poll_votes", id: false, force: :cascade do |t|
    t.integer "topic_id", default: 0, null: false
    t.integer "poll_option_id", limit: 2, null: false
    t.integer "vote_user_id", default: 0, null: false
    t.string "vote_user_ip", limit: 40, default: "", null: false
    t.index ["topic_id"], name: "phpbb_poll_votes_topic_id"
    t.index ["vote_user_id"], name: "phpbb_poll_votes_vote_user_id"
    t.index ["vote_user_ip"], name: "phpbb_poll_votes_vote_user_ip"
  end

  create_table "phpbb_posts", primary_key: "post_id", id: :integer, default: -> { "nextval('phpbb_posts_seq'::regclass)" }, force: :cascade do |t|
    t.integer "topic_id", default: 0, null: false
    t.integer "forum_id", default: 0, null: false
    t.integer "poster_id", default: 0, null: false
    t.integer "icon_id", default: 0, null: false
    t.string "poster_ip", limit: 40, default: "", null: false
    t.integer "post_time", default: 0, null: false
    t.integer "post_reported", limit: 2, null: false
    t.integer "enable_bbcode", limit: 2, null: false
    t.integer "enable_smilies", limit: 2, null: false
    t.integer "enable_magic_url", limit: 2, null: false
    t.integer "enable_sig", limit: 2, null: false
    t.string "post_username", limit: 255, default: "", null: false
    t.string "post_subject", limit: 255, default: "", null: false
    t.text "post_text", default: "", null: false
    t.string "post_checksum", limit: 32, default: "", null: false
    t.integer "post_attachment", limit: 2, null: false
    t.string "bbcode_bitfield", limit: 255, default: "", null: false
    t.string "bbcode_uid", limit: 8, default: "", null: false
    t.integer "post_postcount", limit: 2, null: false
    t.integer "post_edit_time", default: 0, null: false
    t.string "post_edit_reason", limit: 255, default: "", null: false
    t.integer "post_edit_user", default: 0, null: false
    t.integer "post_edit_count", limit: 2, null: false
    t.integer "post_edit_locked", limit: 2, null: false
    t.integer "post_visibility", limit: 2, null: false
    t.integer "post_delete_time", default: 0, null: false
    t.string "post_delete_reason", limit: 255, default: "", null: false
    t.integer "post_delete_user", default: 0, null: false
    t.index ["forum_id"], name: "phpbb_posts_forum_id"
    t.index ["post_username"], name: "phpbb_posts_post_username"
    t.index ["post_visibility"], name: "phpbb_posts_post_visibility"
    t.index ["poster_id"], name: "phpbb_posts_poster_id"
    t.index ["poster_ip"], name: "phpbb_posts_poster_ip"
    t.index ["topic_id", "post_time"], name: "phpbb_posts_tid_post_time"
    t.index ["topic_id"], name: "phpbb_posts_topic_id"
  end

  create_table "phpbb_privmsgs", primary_key: "msg_id", id: :integer, default: -> { "nextval('phpbb_privmsgs_seq'::regclass)" }, force: :cascade do |t|
    t.integer "root_level", default: 0, null: false
    t.integer "author_id", default: 0, null: false
    t.integer "icon_id", default: 0, null: false
    t.string "author_ip", limit: 40, default: "", null: false
    t.integer "message_time", default: 0, null: false
    t.integer "enable_bbcode", limit: 2, null: false
    t.integer "enable_smilies", limit: 2, null: false
    t.integer "enable_magic_url", limit: 2, null: false
    t.integer "enable_sig", limit: 2, null: false
    t.string "message_subject", limit: 255, default: "", null: false
    t.text "message_text", default: "", null: false
    t.string "message_edit_reason", limit: 255, default: "", null: false
    t.integer "message_edit_user", default: 0, null: false
    t.integer "message_attachment", limit: 2, null: false
    t.string "bbcode_bitfield", limit: 255, default: "", null: false
    t.string "bbcode_uid", limit: 8, default: "", null: false
    t.integer "message_edit_time", default: 0, null: false
    t.integer "message_edit_count", limit: 2, null: false
    t.string "to_address", limit: 4000, default: "", null: false
    t.string "bcc_address", limit: 4000, default: "", null: false
    t.integer "message_reported", limit: 2, null: false
    t.index ["author_id"], name: "phpbb_privmsgs_author_id"
    t.index ["author_ip"], name: "phpbb_privmsgs_author_ip"
    t.index ["message_time"], name: "phpbb_privmsgs_message_time"
    t.index ["root_level"], name: "phpbb_privmsgs_root_level"
  end

  create_table "phpbb_privmsgs_folder", primary_key: "folder_id", id: :integer, default: -> { "nextval('phpbb_privmsgs_folder_seq'::regclass)" }, force: :cascade do |t|
    t.integer "user_id", default: 0, null: false
    t.string "folder_name", limit: 255, default: "", null: false
    t.integer "pm_count", default: 0, null: false
    t.index ["user_id"], name: "phpbb_privmsgs_folder_user_id"
  end

  create_table "phpbb_privmsgs_rules", primary_key: "rule_id", id: :integer, default: -> { "nextval('phpbb_privmsgs_rules_seq'::regclass)" }, force: :cascade do |t|
    t.integer "user_id", default: 0, null: false
    t.integer "rule_check", default: 0, null: false
    t.integer "rule_connection", default: 0, null: false
    t.string "rule_string", limit: 255, default: "", null: false
    t.integer "rule_user_id", default: 0, null: false
    t.integer "rule_group_id", default: 0, null: false
    t.integer "rule_action", default: 0, null: false
    t.integer "rule_folder_id", default: 0, null: false
    t.index ["user_id"], name: "phpbb_privmsgs_rules_user_id"
  end

  create_table "phpbb_privmsgs_to", id: false, force: :cascade do |t|
    t.integer "msg_id", default: 0, null: false
    t.integer "user_id", default: 0, null: false
    t.integer "author_id", default: 0, null: false
    t.integer "pm_deleted", limit: 2, null: false
    t.integer "pm_new", limit: 2, null: false
    t.integer "pm_unread", limit: 2, null: false
    t.integer "pm_replied", limit: 2, null: false
    t.integer "pm_marked", limit: 2, null: false
    t.integer "pm_forwarded", limit: 2, null: false
    t.integer "folder_id", default: 0, null: false
    t.index ["author_id"], name: "phpbb_privmsgs_to_author_id"
    t.index ["msg_id"], name: "phpbb_privmsgs_to_msg_id"
    t.index ["user_id", "folder_id"], name: "phpbb_privmsgs_to_usr_flder_id"
  end

  create_table "phpbb_profile_fields", primary_key: "field_id", id: :integer, default: -> { "nextval('phpbb_profile_fields_seq'::regclass)" }, force: :cascade do |t|
    t.string "field_name", limit: 255, default: "", null: false
    t.string "field_type", limit: 100, default: "", null: false
    t.string "field_ident", limit: 20, default: "", null: false
    t.string "field_length", limit: 20, default: "", null: false
    t.string "field_minlen", limit: 255, default: "", null: false
    t.string "field_maxlen", limit: 255, default: "", null: false
    t.string "field_novalue", limit: 255, default: "", null: false
    t.string "field_default_value", limit: 255, default: "", null: false
    t.string "field_validation", limit: 64, default: "", null: false
    t.integer "field_required", limit: 2, null: false
    t.integer "field_show_on_reg", limit: 2, null: false
    t.integer "field_hide", limit: 2, null: false
    t.integer "field_no_view", limit: 2, null: false
    t.integer "field_active", limit: 2, null: false
    t.integer "field_order", default: 0, null: false
    t.integer "field_show_profile", limit: 2, null: false
    t.integer "field_show_on_vt", limit: 2, null: false
    t.integer "field_show_novalue", limit: 2, null: false
    t.integer "field_show_on_pm", limit: 2, null: false
    t.integer "field_show_on_ml", limit: 2, null: false
    t.integer "field_is_contact", limit: 2, null: false
    t.string "field_contact_desc", limit: 255, default: "", null: false
    t.string "field_contact_url", limit: 255, default: "", null: false
    t.index ["field_order"], name: "phpbb_profile_fields_fld_ordr"
    t.index ["field_type"], name: "phpbb_profile_fields_fld_type"
  end

  create_table "phpbb_profile_fields_data", primary_key: "user_id", id: :integer, default: 0, force: :cascade do |t|
    t.text "pf_phpbb_interests", default: "", null: false
    t.text "pf_phpbb_occupation", default: "", null: false
    t.string "pf_phpbb_icq", limit: 255, default: "", null: false
    t.string "pf_phpbb_location", limit: 255, default: "", null: false
    t.string "pf_phpbb_googleplus", limit: 255, default: "", null: false
    t.string "pf_phpbb_youtube", limit: 255, default: "", null: false
    t.string "pf_phpbb_twitter", limit: 255, default: "", null: false
    t.string "pf_phpbb_skype", limit: 255, default: "", null: false
    t.string "pf_phpbb_facebook", limit: 255, default: "", null: false
    t.string "pf_phpbb_website", limit: 255, default: "", null: false
    t.string "pf_phpbb_wlm", limit: 255, default: "", null: false
    t.string "pf_phpbb_yahoo", limit: 255, default: "", null: false
    t.string "pf_phpbb_aol", limit: 255, default: "", null: false
  end

  create_table "phpbb_profile_fields_lang", primary_key: ["field_id", "lang_id", "option_id"], force: :cascade do |t|
    t.integer "field_id", default: 0, null: false
    t.integer "lang_id", default: 0, null: false
    t.integer "option_id", default: 0, null: false
    t.string "field_type", limit: 100, default: "", null: false
    t.string "lang_value", limit: 255, default: "", null: false
  end

  create_table "phpbb_profile_lang", primary_key: ["field_id", "lang_id"], force: :cascade do |t|
    t.integer "field_id", default: 0, null: false
    t.integer "lang_id", default: 0, null: false
    t.string "lang_name", limit: 255, default: "", null: false
    t.string "lang_explain", limit: 4000, default: "", null: false
    t.string "lang_default_value", limit: 255, default: "", null: false
  end

  create_table "phpbb_ranks", primary_key: "rank_id", id: :integer, default: -> { "nextval('phpbb_ranks_seq'::regclass)" }, force: :cascade do |t|
    t.string "rank_title", limit: 255, default: "", null: false
    t.integer "rank_min", default: 0, null: false
    t.integer "rank_special", limit: 2, null: false
    t.string "rank_image", limit: 255, default: "", null: false
  end

  create_table "phpbb_reports", primary_key: "report_id", id: :integer, default: -> { "nextval('phpbb_reports_seq'::regclass)" }, force: :cascade do |t|
    t.integer "reason_id", limit: 2, null: false
    t.integer "post_id", default: 0, null: false
    t.integer "user_id", default: 0, null: false
    t.integer "user_notify", limit: 2, null: false
    t.integer "report_closed", limit: 2, null: false
    t.integer "report_time", default: 0, null: false
    t.text "report_text", default: "", null: false
    t.integer "pm_id", default: 0, null: false
    t.integer "reported_post_enable_bbcode", limit: 2, null: false
    t.integer "reported_post_enable_smilies", limit: 2, null: false
    t.integer "reported_post_enable_magic_url", limit: 2, null: false
    t.text "reported_post_text", default: "", null: false
    t.string "reported_post_uid", limit: 8, default: "", null: false
    t.string "reported_post_bitfield", limit: 255, default: "", null: false
    t.index ["pm_id"], name: "phpbb_reports_pm_id"
    t.index ["post_id"], name: "phpbb_reports_post_id"
  end

  create_table "phpbb_reports_reasons", primary_key: "reason_id", id: :integer, limit: 2, default: -> { "nextval('phpbb_reports_reasons_seq'::regclass)" }, force: :cascade do |t|
    t.string "reason_title", limit: 255, default: "", null: false
    t.text "reason_description", default: "", null: false
    t.integer "reason_order", limit: 2, null: false
  end

  create_table "phpbb_search_results", primary_key: "search_key", id: :string, limit: 32, default: "", force: :cascade do |t|
    t.integer "search_time", default: 0, null: false
    t.text "search_keywords", default: "", null: false
    t.text "search_authors", default: "", null: false
  end

  create_table "phpbb_search_wordlist", primary_key: "word_id", id: :integer, default: -> { "nextval('phpbb_search_wordlist_seq'::regclass)" }, force: :cascade do |t|
    t.string "word_text", limit: 255, default: "", null: false
    t.integer "word_common", limit: 2, null: false
    t.integer "word_count", default: 0, null: false
    t.index ["word_count"], name: "phpbb_search_wordlist_wrd_cnt"
    t.index ["word_text"], name: "phpbb_search_wordlist_wrd_txt", unique: true
  end

  create_table "phpbb_search_wordmatch", id: false, force: :cascade do |t|
    t.integer "post_id", default: 0, null: false
    t.integer "word_id", default: 0, null: false
    t.integer "title_match", limit: 2, null: false
    t.index ["post_id"], name: "phpbb_search_wordmatch_post_id"
    t.index ["word_id", "post_id", "title_match"], name: "phpbb_search_wordmatch_un_mtch", unique: true
    t.index ["word_id"], name: "phpbb_search_wordmatch_word_id"
  end

  create_table "phpbb_sessions", primary_key: "session_id", id: :string, limit: 32, default: "", force: :cascade do |t|
    t.integer "session_user_id", default: 0, null: false
    t.integer "session_last_visit", default: 0, null: false
    t.integer "session_start", default: 0, null: false
    t.integer "session_time", default: 0, null: false
    t.string "session_ip", limit: 40, default: "", null: false
    t.string "session_browser", limit: 150, default: "", null: false
    t.string "session_forwarded_for", limit: 255, default: "", null: false
    t.string "session_page", limit: 255, default: "", null: false
    t.integer "session_viewonline", limit: 2, null: false
    t.integer "session_autologin", limit: 2, null: false
    t.integer "session_admin", limit: 2, null: false
    t.integer "session_forum_id", default: 0, null: false
    t.index ["session_forum_id"], name: "phpbb_sessions_session_fid"
    t.index ["session_time"], name: "phpbb_sessions_session_time"
    t.index ["session_user_id"], name: "phpbb_sessions_session_user_id"
  end

  create_table "phpbb_sessions_keys", primary_key: ["key_id", "user_id"], force: :cascade do |t|
    t.string "key_id", limit: 32, default: "", null: false
    t.integer "user_id", default: 0, null: false
    t.string "last_ip", limit: 40, default: "", null: false
    t.integer "last_login", default: 0, null: false
    t.index ["last_login"], name: "phpbb_sessions_keys_last_login"
  end

  create_table "phpbb_sitelist", primary_key: "site_id", id: :integer, default: -> { "nextval('phpbb_sitelist_seq'::regclass)" }, force: :cascade do |t|
    t.string "site_ip", limit: 40, default: "", null: false
    t.string "site_hostname", limit: 255, default: "", null: false
    t.integer "ip_exclude", limit: 2, null: false
  end

  create_table "phpbb_smilies", primary_key: "smiley_id", id: :integer, default: -> { "nextval('phpbb_smilies_seq'::regclass)" }, force: :cascade do |t|
    t.string "code", limit: 50, default: "", null: false
    t.string "emotion", limit: 50, default: "", null: false
    t.string "smiley_url", limit: 50, default: "", null: false
    t.integer "smiley_width", limit: 2, null: false
    t.integer "smiley_height", limit: 2, null: false
    t.integer "smiley_order", default: 0, null: false
    t.integer "display_on_posting", limit: 2, null: false
    t.index ["display_on_posting"], name: "phpbb_smilies_display_on_post"
  end

  create_table "phpbb_styles", primary_key: "style_id", id: :integer, default: -> { "nextval('phpbb_styles_seq'::regclass)" }, force: :cascade do |t|
    t.string "style_name", limit: 255, default: "", null: false
    t.string "style_copyright", limit: 255, default: "", null: false
    t.integer "style_active", limit: 2, null: false
    t.string "style_path", limit: 100, default: "", null: false
    t.string "bbcode_bitfield", limit: 255, default: "kNg=", null: false
    t.integer "style_parent_id", default: 0, null: false
    t.string "style_parent_tree", limit: 8000, default: "", null: false
    t.index ["style_name"], name: "phpbb_styles_style_name", unique: true
  end

  create_table "phpbb_teampage", primary_key: "teampage_id", id: :integer, default: -> { "nextval('phpbb_teampage_seq'::regclass)" }, force: :cascade do |t|
    t.integer "group_id", default: 0, null: false
    t.string "teampage_name", limit: 255, default: "", null: false
    t.integer "teampage_position", default: 0, null: false
    t.integer "teampage_parent", default: 0, null: false
  end

  create_table "phpbb_topics", primary_key: "topic_id", id: :integer, default: -> { "nextval('phpbb_topics_seq'::regclass)" }, force: :cascade do |t|
    t.integer "forum_id", default: 0, null: false
    t.integer "icon_id", default: 0, null: false
    t.integer "topic_attachment", limit: 2, null: false
    t.integer "topic_reported", limit: 2, null: false
    t.string "topic_title", limit: 255, default: "", null: false
    t.integer "topic_poster", default: 0, null: false
    t.integer "topic_time", default: 0, null: false
    t.integer "topic_time_limit", default: 0, null: false
    t.integer "topic_views", default: 0, null: false
    t.integer "topic_status", limit: 2, null: false
    t.integer "topic_type", limit: 2, null: false
    t.integer "topic_first_post_id", default: 0, null: false
    t.string "topic_first_poster_name", limit: 255, default: "", null: false
    t.string "topic_first_poster_colour", limit: 6, default: "", null: false
    t.integer "topic_last_post_id", default: 0, null: false
    t.integer "topic_last_poster_id", default: 0, null: false
    t.string "topic_last_poster_name", limit: 255, default: "", null: false
    t.string "topic_last_poster_colour", limit: 6, default: "", null: false
    t.string "topic_last_post_subject", limit: 255, default: "", null: false
    t.integer "topic_last_post_time", default: 0, null: false
    t.integer "topic_last_view_time", default: 0, null: false
    t.integer "topic_moved_id", default: 0, null: false
    t.integer "topic_bumped", limit: 2, null: false
    t.integer "topic_bumper", default: 0, null: false
    t.string "poll_title", limit: 255, default: "", null: false
    t.integer "poll_start", default: 0, null: false
    t.integer "poll_length", default: 0, null: false
    t.integer "poll_max_options", limit: 2, null: false
    t.integer "poll_last_vote", default: 0, null: false
    t.integer "poll_vote_change", limit: 2, null: false
    t.integer "topic_visibility", limit: 2, null: false
    t.integer "topic_delete_time", default: 0, null: false
    t.string "topic_delete_reason", limit: 255, default: "", null: false
    t.integer "topic_delete_user", default: 0, null: false
    t.integer "topic_posts_approved", default: 0, null: false
    t.integer "topic_posts_unapproved", default: 0, null: false
    t.integer "topic_posts_softdeleted", default: 0, null: false
    t.index ["forum_id", "topic_last_post_time", "topic_moved_id"], name: "phpbb_topics_fid_time_moved"
    t.index ["forum_id", "topic_type"], name: "phpbb_topics_forum_id_type"
    t.index ["forum_id", "topic_visibility", "topic_last_post_id"], name: "phpbb_topics_forum_vis_last"
    t.index ["forum_id"], name: "phpbb_topics_forum_id"
    t.index ["topic_last_post_time"], name: "phpbb_topics_last_post_time"
    t.index ["topic_visibility"], name: "phpbb_topics_topic_visibility"
  end

  create_table "phpbb_topics_posted", primary_key: ["user_id", "topic_id"], force: :cascade do |t|
    t.integer "user_id", default: 0, null: false
    t.integer "topic_id", default: 0, null: false
    t.integer "topic_posted", limit: 2, null: false
  end

  create_table "phpbb_topics_track", primary_key: ["user_id", "topic_id"], force: :cascade do |t|
    t.integer "user_id", default: 0, null: false
    t.integer "topic_id", default: 0, null: false
    t.integer "forum_id", default: 0, null: false
    t.integer "mark_time", default: 0, null: false
    t.index ["forum_id"], name: "phpbb_topics_track_forum_id"
    t.index ["topic_id"], name: "phpbb_topics_track_topic_id"
  end

  create_table "phpbb_topics_watch", id: false, force: :cascade do |t|
    t.integer "topic_id", default: 0, null: false
    t.integer "user_id", default: 0, null: false
    t.integer "notify_status", limit: 2, null: false
    t.index ["notify_status"], name: "phpbb_topics_watch_notify_stat"
    t.index ["topic_id"], name: "phpbb_topics_watch_topic_id"
    t.index ["user_id"], name: "phpbb_topics_watch_user_id"
  end

  create_table "phpbb_user_group", id: false, force: :cascade do |t|
    t.integer "group_id", default: 0, null: false
    t.integer "user_id", default: 0, null: false
    t.integer "group_leader", limit: 2, null: false
    t.integer "user_pending", limit: 2, null: false
    t.index ["group_id"], name: "phpbb_user_group_group_id"
    t.index ["group_leader"], name: "phpbb_user_group_group_leader"
    t.index ["user_id"], name: "phpbb_user_group_user_id"
  end

  create_table "phpbb_user_notifications", id: false, force: :cascade do |t|
    t.string "item_type", limit: 255, default: "", null: false
    t.integer "item_id", default: 0, null: false
    t.integer "user_id", default: 0, null: false
    t.string "method", limit: 255, default: "", null: false
    t.integer "notify", limit: 2, null: false
  end

  create_table "phpbb_users", primary_key: "user_id", id: :integer, default: -> { "nextval('phpbb_users_seq'::regclass)" }, force: :cascade do |t|
    t.integer "user_type", limit: 2, null: false
    t.integer "group_id", default: 3, null: false
    t.text "user_permissions", default: "", null: false
    t.integer "user_perm_from", default: 0, null: false
    t.string "user_ip", limit: 40, default: "", null: false
    t.integer "user_regdate", default: 0, null: false
    t.string "username", default: "", null: false
    t.string "username_clean", default: "", null: false
    t.string "user_password", limit: 255, default: "", null: false
    t.integer "user_passchg", default: 0, null: false
    t.string "user_email", limit: 100, default: "", null: false
    t.bigint "user_email_hash", default: 0, null: false
    t.string "user_birthday", limit: 10, default: "", null: false
    t.integer "user_lastvisit", default: 0, null: false
    t.integer "user_lastmark", default: 0, null: false
    t.integer "user_lastpost_time", default: 0, null: false
    t.string "user_lastpage", limit: 200, default: "", null: false
    t.string "user_last_confirm_key", limit: 10, default: "", null: false
    t.integer "user_last_search", default: 0, null: false
    t.integer "user_warnings", limit: 2, null: false
    t.integer "user_last_warning", default: 0, null: false
    t.integer "user_login_attempts", limit: 2, null: false
    t.integer "user_inactive_reason", limit: 2, null: false
    t.integer "user_inactive_time", default: 0, null: false
    t.integer "user_posts", default: 0, null: false
    t.string "user_lang", limit: 30, default: "", null: false
    t.string "user_timezone", limit: 100, default: "", null: false
    t.string "user_dateformat", limit: 64, default: "d M Y H:i", null: false
    t.integer "user_style", default: 0, null: false
    t.integer "user_rank", default: 0, null: false
    t.string "user_colour", limit: 6, default: "", null: false
    t.integer "user_new_privmsg", default: 0, null: false
    t.integer "user_unread_privmsg", default: 0, null: false
    t.integer "user_last_privmsg", default: 0, null: false
    t.integer "user_message_rules", limit: 2, null: false
    t.integer "user_full_folder", default: -3, null: false
    t.integer "user_emailtime", default: 0, null: false
    t.integer "user_topic_show_days", limit: 2, null: false
    t.string "user_topic_sortby_type", limit: 1, default: "t", null: false
    t.string "user_topic_sortby_dir", limit: 1, default: "d", null: false
    t.integer "user_post_show_days", limit: 2, null: false
    t.string "user_post_sortby_type", limit: 1, default: "t", null: false
    t.string "user_post_sortby_dir", limit: 1, default: "a", null: false
    t.integer "user_notify", limit: 2, null: false
    t.integer "user_notify_pm", limit: 2, null: false
    t.integer "user_notify_type", limit: 2, null: false
    t.integer "user_allow_pm", limit: 2, null: false
    t.integer "user_allow_viewonline", limit: 2, null: false
    t.integer "user_allow_viewemail", limit: 2, null: false
    t.integer "user_allow_massemail", limit: 2, null: false
    t.integer "user_options", default: 230271, null: false
    t.string "user_avatar", limit: 255, default: "", null: false
    t.string "user_avatar_type", limit: 255, default: "", null: false
    t.integer "user_avatar_width", limit: 2, null: false
    t.integer "user_avatar_height", limit: 2, null: false
    t.text "user_sig", default: "", null: false
    t.string "user_sig_bbcode_uid", limit: 8, default: "", null: false
    t.string "user_sig_bbcode_bitfield", limit: 255, default: "", null: false
    t.string "user_jabber", limit: 255, default: "", null: false
    t.string "user_actkey", limit: 32, default: "", null: false
    t.string "user_newpasswd", limit: 255, default: "", null: false
    t.string "user_form_salt", limit: 32, default: "", null: false
    t.integer "user_new", limit: 2, null: false
    t.integer "user_reminded", limit: 2, null: false
    t.integer "user_reminded_time", default: 0, null: false
    t.index ["user_birthday"], name: "phpbb_users_user_birthday"
    t.index ["user_email_hash"], name: "phpbb_users_user_email_hash"
    t.index ["user_type"], name: "phpbb_users_user_type"
    t.index ["username_clean"], name: "phpbb_users_username_clean", unique: true
  end

  create_table "phpbb_warnings", primary_key: "warning_id", id: :integer, default: -> { "nextval('phpbb_warnings_seq'::regclass)" }, force: :cascade do |t|
    t.integer "user_id", default: 0, null: false
    t.integer "post_id", default: 0, null: false
    t.integer "log_id", default: 0, null: false
    t.integer "warning_time", default: 0, null: false
  end

  create_table "phpbb_words", primary_key: "word_id", id: :integer, default: -> { "nextval('phpbb_words_seq'::regclass)" }, force: :cascade do |t|
    t.string "word", limit: 255, default: "", null: false
    t.string "replacement", limit: 255, default: "", null: false
  end

  create_table "phpbb_zebra", primary_key: ["user_id", "zebra_id"], force: :cascade do |t|
    t.integer "user_id", default: 0, null: false
    t.integer "zebra_id", default: 0, null: false
    t.integer "friend", limit: 2, null: false
    t.integer "foe", limit: 2, null: false
  end

  create_table "posts", force: :cascade do |t|
    t.string "name"
    t.string "title"
    t.text "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "visitors", force: :cascade do |t|
    t.string "name"
    t.string "lastname"
    t.string "login"
    t.string "password"
    t.string "email"
    t.string "phone"
    t.string "tip"
    t.text "education"
    t.text "occupation"
    t.string "subsection"
    t.float "rating"
    t.datetime "injob"
    t.datetime "outjob"
    t.datetime "startmeeting"
    t.datetime "endmeeting"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "profile"
    t.string "avatar_file_name"
    t.string "avatar_content_type"
    t.integer "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.index ["email"], name: "index_visitors_on_email", unique: true
    t.index ["reset_password_token"], name: "index_visitors_on_reset_password_token", unique: true
  end

  create_table "works", force: :cascade do |t|
    t.string "name"
    t.integer "nomer"
    t.text "descr"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.index ["reset_password_token"], name: "index_works_on_reset_password_token", unique: true
  end

end

class CreateEmployees < ActiveRecord::Migration[5.1]
  def change
    create_table :employees do |t|
      t.has_attached_file :profile
      t.string :name
      t.string :lastname
      t.string :login
      t.string :password
      t.string :email
      t.string :phone
      t.string :accesslevel

      t.timestamps
    end
  end
end

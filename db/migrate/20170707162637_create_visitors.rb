class CreateVisitors < ActiveRecord::Migration[5.1]
  def change
    create_table :visitors do |t|
      t.string :name
      t.string :lastname
      t.string :login
      t.string :password
      t.string :email
      t.string :phone
      t.string :tip
      t.text :education
      t.text :occupation
      t.string :subsection
      t.float :rating
      t.datetime :injob
      t.datetime :outjob
      t.datetime :startmeeting
      t.datetime :endmeeting

      t.timestamps
    end
  end
end

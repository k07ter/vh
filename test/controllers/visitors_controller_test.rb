require 'test_helper'

class VisitorsControllerTest < ActionDispatch::IntegrationTest
  test "should get profile" do
    get visitors_profile_url
    assert_response :success
  end

end

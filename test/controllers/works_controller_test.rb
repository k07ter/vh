require 'test_helper'

class WorksControllerTest < ActionDispatch::IntegrationTest
  test "should get profile" do
    get works_profile_url
    assert_response :success
  end

end

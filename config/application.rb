require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Cabinet
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.1
    config.action_controller.forgery_protection_origin_check = false

    config.assets.paths << Rails.root.join( 'app','views','webinary','ring' )
    config.time_zone = 'Europe/Moscow'

    I18n.enforce_available_locales = true
    config.i18n.available_locales = [:ru, :en]
    config.i18n.default_locale = :ru

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
  end
end

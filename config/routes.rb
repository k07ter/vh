Rails.application.routes.draw do
  resources :meetings
  #resources :posts

  get 'webinary/index'
  get 'services/index'
  get 'jobs/index'
  get 'employees/profile'

  devise_for :visitors
  devise_for :employees
  resources :employees
  devise_for :users
  devise_for :works

  get 'visitors/profile'
  get 'works/profile'
  get 'meetings/index'

  resources :visitors
  resources :works
  get 'home/index', as: 'employee_root'

  root :to => 'home#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
